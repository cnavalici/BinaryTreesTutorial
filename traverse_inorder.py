#!/usr/bin/env python3

#           1
#         /   \
#        2     4
#      /   \  /  \
#     5    6  10  12
#    /            / \
#   8            9   3

from src.TreeBuilder import build_tree

print("Inorder traversal")
print("Recursive traversal of the left node, then root, then recursive traversal of the right node.")

print("Build the tree")
root = build_tree()


def inorder(node):
    if node:
        inorder(node.left)
        print(node)
        inorder(node.right)


# 8 5 2 6 1 10 4 9 12 3
print("Run the INORDER traversal")
inorder(root)